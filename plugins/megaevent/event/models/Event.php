<?php namespace MegaEvent\Event\Models;

use Model;

/**
 * Model
 */
class Event extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;


    /**
     * @var string The database table used by the model.
     */
    public $table = 'megaevent_event_events';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];
    // public $belongsTo = [
    //     'tag' => 'MegaEvent\Event\Models\Tag'
    // ];
    // public $belongsTo = [
    //     'category' => 'MegaEvent\Event\Models\Category'
    // ];
    // public $belongsTo = [
    //     'venu' => 'MegaEvent\Event\Models\Venu'
    // ];
    // public $belongsTo = [
    //     'organizer' => 'MegaEvent\Event\Models\Organizer'
    // ];

    public $belongsTo = [
        'tag' => [
            'MegaEvent\Event\Models\Tag',
        ],
        'category' => [
            'MegaEvent\Event\Models\Category',
        ],
        'venu' => [
            'MegaEvent\Event\Models\Venu',
        ]
    ];
     public $belongsToMany = [
        'organizer' =>  [
            'MegaEvent\Event\Models\Organizer',
            'table'=>'megaevent_event_events_organizers',
            'order'=>'name'
        ]
    ];
}
