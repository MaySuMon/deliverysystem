<?php namespace MegaEvent\Event\Models;

use Model;

/**
 * Model
 */
class Tag extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;


    /**
     * @var string The database table used by the model.
     */
    public $table = 'megaevent_event_tags';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];
}
