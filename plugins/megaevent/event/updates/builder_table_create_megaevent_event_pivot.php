<?php namespace MegaEvent\Event\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateMegaeventEventPivot extends Migration
{
    public function up()
    {
        Schema::create('megaevent_event_pivot', function($table)
        {
            $table->engine = 'InnoDB';
            $table->integer('event_id')->unsigned();
            $table->integer('category_id')->unsigned();
            $table->primary(['event_id','category_id']);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('megaevent_event_pivot');
    }
}
