<?php namespace MegaEvent\Event\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateMegaeventEventOrganizers extends Migration
{
    public function up()
    {
        Schema::create('megaevent_event_organizers', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('name');
            $table->text('description');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('megaevent_event_organizers');
    }
}
