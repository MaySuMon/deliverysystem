<?php namespace MegaEvent\Event\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateMegaeventEventPivot extends Migration
{
    public function up()
    {
        Schema::table('megaevent_event_pivot', function($table)
        {
            $table->integer('event_id')->unsigned(false)->change();
            $table->integer('category_id')->unsigned(false)->change();
        });
    }
    
    public function down()
    {
        Schema::table('megaevent_event_pivot', function($table)
        {
            $table->integer('event_id')->unsigned()->change();
            $table->integer('category_id')->unsigned()->change();
        });
    }
}
