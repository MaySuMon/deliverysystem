<?php namespace MegaEvent\Event\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateMegaeventEventEvents extends Migration
{
    public function up()
    {
        Schema::create('megaevent_event_events', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('name');
            $table->integer('tag_id');
            $table->integer('category_id');
            $table->integer('venu_id');
            $table->integer('organizer_id');
            $table->dateTime('from_date');
            $table->dateTime('to_date');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('megaevent_event_events');
    }
}
