<?php namespace MegaEvent\Event\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateMegaeventEventVenu extends Migration
{
    public function up()
    {
        Schema::create('megaevent_event_venu', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('name');
            $table->text('description')->nullable();
            $table->text('address');
            $table->string('city');
            $table->string('state');
            $table->string('postal_code');
            $table->string('phone');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('megaevent_event_venu');
    }
}
