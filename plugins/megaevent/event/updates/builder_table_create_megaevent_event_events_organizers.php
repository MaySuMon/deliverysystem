<?php namespace MegaEvent\Event\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateMegaeventEventEventsOrganizers extends Migration
{
    public function up()
    {
        Schema::create('megaevent_event_events_organizers', function($table)
        {
            $table->engine = 'InnoDB';
            $table->integer('event_id');
            $table->integer('organizer_id');
            $table->primary(['event_id','organizer_id']);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('megaevent_event_events_organizers');
    }
}
