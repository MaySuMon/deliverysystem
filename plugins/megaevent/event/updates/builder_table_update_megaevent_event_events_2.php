<?php namespace MegaEvent\Event\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateMegaeventEventEvents2 extends Migration
{
    public function up()
    {
        Schema::table('megaevent_event_events', function($table)
        {
            $table->renameColumn('organizer_id', 'category_id');
        });
    }
    
    public function down()
    {
        Schema::table('megaevent_event_events', function($table)
        {
            $table->renameColumn('category_id', 'organizer_id');
        });
    }
}
