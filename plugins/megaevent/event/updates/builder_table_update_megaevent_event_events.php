<?php namespace MegaEvent\Event\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateMegaeventEventEvents extends Migration
{
    public function up()
    {
        Schema::table('megaevent_event_events', function($table)
        {
            $table->dropColumn('category_id');
        });
    }
    
    public function down()
    {
        Schema::table('megaevent_event_events', function($table)
        {
            $table->integer('category_id');
        });
    }
}
